// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// Returns the vector if all are ok, or the first found error.
pub fn all_ok<'a, I, V: 'a, R, E, F>(values: I, f: F) -> Result<Vec<R>, E>
where F: Fn(&V) -> Result<R, E>,
      I: Iterator<Item = &'a V> {
    let mut result = Vec::new();
    for value in values {
        match f(value) {
            Ok(r) => {
                result.push(r);
            },
            Err(e) => {
                return Err(e);
            }
        }
    }
    Ok(result)
}
